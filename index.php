<?php include 'header.php';?>




<body>


   <!-- Start hero slider/ Owl Carousel Slider -->
   <div class="tw-hero-slider owl-carousel">

      <div class="slider-1 slider-map-pattern">
         <!-- Slider arrow end -->
         <div class="slider-wrapper d-table">
            <div class="slider-inner d-table-cell">
               <div class="container">
                  <div class="row justify-content-center">
                     <div class="col-md-12">
                        <img src="images/slider/slider1.png" alt="" class="slider-img img-fluid">
                     </div>
                     <!-- Col End -->
                  </div>
                  <!-- Row End -->
                  <div class="row justify-content-center text-center">
                     <div class="col-md-10">
                        <h1 class="tw-slider-subtitle">Search Engine Optimization</h1>
                        <h4 class="tw-slider-title">Digital Marketing
                           <span>Solution</span>
                        </h4>
                     </div>
                     <!-- End Col -->
                  </div>
                  <!-- Row End -->
               </div>
               <!-- Container End -->
            </div>
            <!-- Slider Inner End -->
         </div>
         <!-- Slider Wrapper End -->
      </div>
      <!-- Slider 1 end -->


      <div class="slider-2">
         <div class="slider-arrow">
            <img src="images/slider/pattern_arrow2.png" alt="sliderArrow1">
            <img src="images/slider/pattern_arrow1.png" alt="sliderArrow2">
            <img src="images/slider/pattern_arrow3.png" alt="sliderArrow3">
         </div>
         <div class="slider-wrapper d-table">
            <div class="slider-inner d-table-cell">
               <div class="container">
                  <div class="row justify-content-center">
                     <div class="col-md-6">
                        <div class="slider-content">
                           <h1>Website Design &
                              <span>Development</span>
                           </h1>
                           <p>Design your success, creative web services<br>Attractive website solutions to meet your demands–Fits nicely into your business</p>
                           <a href="#" class="btn btn-dark">Free Analysis</a>
                        </div>
                     </div>
                     <!-- Col end -->
                     <div class="col-md-6">
                        <img src="images/slider/slider2.png" alt="" class="img-fluid slider-img">
                     </div>
                     <!-- col end -->
                  </div>
                  <!-- Row End -->
               </div>
               <!-- Container End -->
            </div>
            <!-- Slider Inner End -->
         </div>
         <!-- Slider Wrapper End -->
      </div>
      <!-- Slider 2 end -->


      <div class="slider-3">
         <div class="slider-arrow">
            <img src="images/slider/pattern_arrow2.png" alt="sliderArrow1">
            <img src="images/slider/pattern_arrow1.png" alt="sliderArrow2">
            <img src="images/slider/pattern_arrow3.png" alt="sliderArrow3">
         </div>
         <div class="slider-wrapper d-table">
            <div class="slider-inner d-table-cell">
               <div class="container">
                  <div class="row justify-content-center">
                     <div class="col-md-6">
                        <img src="images/slider/slider3.png" alt="" class="img-fluid slider-img">
                     </div>
                     <!-- Col end -->

                     <div class="col-md-6">
                        <div class="slider-content">
                           <h1>Social Media Optimization</h1>
                           <p>Double the pleasure, double the exposure <br>Make your business grow with our autonomous social media strategy</p>
                           <a href="#" class="btn btn-dark">Free Analysis</a>
                        </div>
                        <!-- End Slider Content -->
                     </div>
                     <!-- col end -->
                  </div>
                  <!-- Row End -->
               </div>
               <!-- COntainer End -->
            </div>
            <!-- Slider Inner End -->
         </div>
         <!-- Slider Wrapper End -->
      </div>
      <!-- Slider 3 end -->
   </div>
   <!-- End Carousel -->


   <section id="tw-features" class="tw-features-area">
      <div class="container">
         <div class="row tw-mb-65">
            <div class="col-md-2 wow fadeInLeft" data-wow-duration="1s">
               <h2 class="column-title text-md-right text-sm-center">Who We Are</h2>
            </div>
            <!-- Col End -->
            <div class="col-md-9 ml-md-auto wow fadeInRight" data-wow-duration="1s">
               <p class="features-text">We see ourselves as the first generation of digital companies, bridging the gap between marketing and technology and making decisions led by data and experience. We have spent our best years developing newer and better ways of serving you, which is the best for your business and brand. By addressing key areas like website design and development, content creation and digital marketing, we provide you the best opportunity to spice up your business online.We not only design your website; we also optimize it to appear on top of Google search results. Our websites do a wonderful job at communicating your brand message to your audience.

</p>
            </div>
            <!-- Col End -->
         </div>
      </div>
      <!-- End Container -->
   </section>
   <!-- End Features section -->


   <section id="tw-analysis" class="tw-analysis-area">
      <div class="analysis-bg-pattern d-none d-md-inline-block">
         <img class="wow fadeInUp" src="images/check-seo/cloud.png" alt="">
         <img class="wow fadeInUp" src="images/check-seo/cloud2.png" alt="">
         <img class="wow fadeInUp" src="images/check-seo/announce.png" alt="">
         <img class="wow fadeInUp" src="images/check-seo/chart.png" alt="">
      </div>
      <!-- End Analysis Pattern img -->
      <div class="container">
         <div class="row justify-content-center">
            <div class="col-md-8 text-center wow fadeInDown">
               <h2 class="column-title">
                  Enquire
                  <span class="text-white">Now</span>
               </h2>
               <div class="analysis-form">
                  <form class="form-vertical">
                     <div class="row justify-content-center">
                        <div class="col-lg-4 col-md-12 no-padding">
                           <div class="form-group tw-form-round-shape">
                              <input type="url" id="url" name="url" placeholder="Your Name" class="form-control">
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-12 no-padding">
                           <div class="form-group tw-form-round-shape">
                              <input type="email" id="email" name="email" placeholder="Your Mobile Number" class="form-control">
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-12 no-padding">
                           <div class="form-group">
                              <input type="submit" value="submit now">
                           </div>
                        </div>
                     </div>
                  </form>
                  <!-- End Form -->
               </div>
               <!-- End Analysis form -->
            </div>
            <!-- Col End -->
         </div>
         <!-- End Row -->
      </div>
      <!-- End container -->
   </section>
   <!-- End Analysis Section -->


   <section id="tw-intro" class="tw-intro-area">
      <div class="tw-ellipse-pattern">
         <img src="images/about/about_ellipse.png" alt="">
      </div>
      <!-- End Ellipse Pattern -->
      <div class="container">
         <div class="row">

            <div class="col-lg-6 col-md-12 text-lg-right text-md-center wow fadeInLeft" data-wow-duration="1s">
               <img src="images/about/about_image.png" alt="" class="img-fluid">
            </div>
            <!-- End Col -->
            <div class="col-lg-6 ml-auto col-md-12 wow fadeInRight" data-wow-duration="1s">
               <h2 class="column-title">Works for Advertiser & Publisher</h2>
               <span class="animate-border tw-mb-40"></span>
               <p>
                  In the age of multi-screen and multi-tasking we are helping advertisers to do cross channel advertising thereby increasing their campaign effectiveness and minimizing ad waste. Advertising across multiple channels provides actionable context and drive effectiveness. We would be happy to demonstrate as how can we add value & optimize your digital branding spends.
               </p>
               <p>
                 Monetize your traffic with India's leading digital branding agency. Pink Pixels helps its publishers to maximize the monetization of their VCPMS across devices. We provide publishers with our proprietary technology that optimizes your earning potential across the global list of available campaigns. 
               </p>
               <!-- End Intro list -->
               <a href="#" class="btn btn-primary">Advertiser Login</a>
               <!-- Default Round Btn -->
               <a href="#" class="btn btn-secondary">Publisher Login</a>
            </div>
            <!-- End Col -->
         </div>
         <!-- End Row -->
      </div>
      <!-- End Container -->
   </section>
   <!-- Intro section End -->


   <section id="tw-service" class="tw-service">
      <div class="container">
         <div class="row text-center">
            <div class="col section-heading wow fadeInDown">
               <h2>
                  <small>Our services</small>
                  Our Best
                  <span>Services</span>
               </h2>
               <span class="animate-border ml-auto mr-auto tw-mt-20"></span>
            </div>
            <!-- Title Col End -->
         </div>
         <!-- Title Row End -->
         <div class="row">

		            <div class="col-lg-3 col-md-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".2s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="images/icon/feature1.png" alt="">
                     </div>
                     <!-- End Features icon inner -->
                  </div>
                  <!-- End Features Icon -->
                  <h3>Website Design</h3>
                  <p>Bringing your ideas to live, website designs are our forte. We carve perfect designs to build creative websites that engage users on both desktop & mobile devices with its ultra-modern.
                  </p>
                  <a href="web-design" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
               <!-- End Single Features -->
            </div>
            <!-- Col End -->
            <div class="col-lg-3 col-md-12  wow fadeInUp" data-wow-duration="1.6s" data-wow-delay=".4s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="images/icon/feature2.png" alt="">
                     </div>
                     <!-- End Features Icon inner -->
                  </div>
                  <!-- End Features Icon -->
                  <h3>Web Development</h3>
                  <p>Our team of experts builds intelligent websites that actually perform & exceed your expectations. We offer completely customized interactive web solutions.
                  </p>
                  <a href="web-development" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
               <!-- End Single Features -->
            </div>
            <!-- end col -->
            <div class="col-lg-3 col-md-12  wow fadeInUp" data-wow-duration="1.9s" data-wow-delay=".6s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="images/icon/feature3.png" alt="">
                     </div>
                     <!-- End Features Icon inner -->
                  </div>
                  <!-- End Features Icon -->
                  <h3>Software Development</h3>
                  <p>Star Web Maker Services Pvt.Ltd has evolved as a true professional software company after successfully completing numerous client projects. We develop solutions that best meet business objectives.
                  </p>
                  <a href="software-development" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
               <!-- End Single Features -->
            </div>
			<!-- end col --> 
			<div class="col-lg-3 col-md-12  wow fadeInUp" data-wow-duration="1.9s" data-wow-delay=".6s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="images/icon/feature3.png" alt="">
                     </div>
                     <!-- End Features Icon inner -->
                  </div>
                  <!-- End Features Icon -->
                  <h3>App Development</h3>
                  <p>Star Web Maker is one of the best enterprise mobile application development company in India. Our expertise lies in building Android and iOS applications supported by a strong cloud based backend infrastructure.
                  </p>
                  <a href="app-development" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
               <!-- End Single Features -->
            </div>
            <!-- Col End -->
            <div class="col-lg-3 col-md-12  wow fadeInUp" data-wow-duration="1.6s" data-wow-delay=".4s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="images/icon/feature2.png" alt="">
                     </div>
                     <!-- End Features Icon inner -->
                  </div>
                  <!-- End Features Icon -->
                  <h3>SEO Services</h3>
                  <p>With an SEO strategy combining technical, content and authority optimization, we ensure your website ranks well in the SERPs.</p>
                  <a href="seo-services" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
               <!-- End Single Features -->
            </div>
            <!-- end col -->
            <div class="col-lg-3 col-md-12  wow fadeInUp" data-wow-duration="1.9s" data-wow-delay=".6s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="images/icon/feature3.png" alt="">
                     </div>
                     <!-- End Features Icon inner -->
                  </div>
                  <!-- End Features Icon -->
                  <h3>Social Media Marketing</h3>
                  <p>With effective social activity, we help brands reach, engage and convert the right audience online.</p>
                  <a href="social-media-marketing" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
               <!-- End Single Features -->
            </div>
			<!-- end col --> 
			<div class="col-lg-3 col-md-12  wow fadeInUp" data-wow-duration="1.9s" data-wow-delay=".6s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="images/icon/feature3.png" alt="">
                     </div>
                     <!-- End Features Icon inner -->
                  </div>
                  <!-- End Features Icon -->
                  <h3>Pay Per Click</h3>
                  <p>PPC is a comprehensive online advertising method which is being included with Bing Ads or Google AdWords.</p>
                  <a href="pay-per-click" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
               <!-- End Single Features -->
            </div>
			<!-- end col --> 
			<div class="col-lg-3 col-md-12  wow fadeInUp" data-wow-duration="1.9s" data-wow-delay=".6s">
               <div class="features-box">
                  <div class="features-icon d-table">
                     <div class="features-icon-inner d-table-cell">
                        <img src="images/icon/feature3.png" alt="">
                     </div>
                     <!-- End Features Icon inner -->
                  </div>
                  <!-- End Features Icon -->
                  <h3>E-mail Marketing</h3>
                  <p>Start working with an company that provide everything you need to any create awareness drive
                  </p>
                  <a href="e-mail-marketing" class="tw-readmore">Read More
                     <i class="fa fa-angle-right"></i>
                  </a>
               </div>
               <!-- End Single Features -->
            </div>
			 
         </div>
      </div>
      <!-- container -->
   </section>
   <!-- Tw Service End -->




   <section id="work-process" class="work-process">
      <div class="work-bg-pattern d-none d-lg-inline-block">
         <img src="images/process/work_process.png" alt="" class="img-fluid wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1.2s">
      </div>
      <!-- End Work BG Pattern -->
      <div class="container">
         <div class="row text-center">
            <div class="col section-heading wow fadeInDown" data-wow-duration="1s">
               <h2>
                  <small>easy steps</small>Our Working <span>Process</span></h2>
               <span class="animate-border ml-auto mr-auto tw-mt-20"></span>
            </div>
            <!-- End Col -->
         </div>
         <!-- End Row -->
         <div class="row">
            <div class="col-md-3">
               <div class="tw-work-process">
                  <div class="process-wrapper d-table wow zoomIn" data-wow-duration="1s" data-wow-delay=".2s">
                     <div class="process-inner d-table-cell">
                        <img src="images/icon/process1.png" alt="" class="img-fluid">
                     </div>
                  </div>
                  <!-- End process wrapper -->
                  <p>01. Research Project</p>
               </div>
               <!-- End Tw work process -->
            </div>
            <!-- End Col -->
            <div class="col-md-3">
               <div class="tw-work-process">
                  <div class="process-wrapper bg-orange d-table wow zoomIn" data-wow-duration="1s" data-wow-delay=".4s">
                     <div class="process-inner d-table-cell">
                        <img src="images/icon/process2.png" alt="" class="img-fluid">
                     </div>
                  </div>
                  <!-- End Process Wrapper -->
                  <p>02. Find Ideas</p>
               </div>
               <!-- End Word Process -->
            </div>
            <!-- End Col -->
            <div class="col-md-3">
               <div class="tw-work-process">
                  <div class="process-wrapper bg-blue d-table wow zoomIn" data-wow-duration="1s" data-wow-delay=".6s">
                     <div class="process-inner d-table-cell">
                        <img src="images/icon/process3.png" alt="" class="img-fluid">
                     </div>
                  </div>
                  <!-- End Process Wrapper -->
                  <p>03. Start Optimize</p>
               </div>
               <!-- End Work Process -->
            </div>
            <!-- End Col -->
            <div class="col-md-3">
               <div class="tw-work-process">
                  <div class="process-wrapper bg-yellow d-table wow zoomIn" data-wow-duration="1s" data-wow-delay=".8s">
                     <div class="process-inner d-table-cell">
                        <img src="images/icon/process4.png" alt="" class="img-fluid">
                     </div>
                  </div>
                  <!-- End PRocess Wrapper -->
                  <p>04. Reach Target</p>
               </div>
               <!-- End Work Process -->
            </div>
         </div>
         <!-- End Row -->
      </div>
      <!-- End Container -->
   </section>
   <!-- End Word Process -->
	
	
   <section id="tw-facts" class="tw-facts">
      <div class="facts-bg-pattern d-none d-lg-block">
         <img class="wow fadeInLeft" src="images/funfacts/arrow_left.png" alt="arrwo_left">
         <img class="wow fadeInRight" src="images/funfacts/arrow_right.png" alt="arrow_right">
      </div>
      <div class="container">
         <div class="row">
            <div class="col-md-3 text-center">
               <div class="tw-facts-box">
                  <div class="facts-img wow zoomIn" data-wow-duration="1s">
                     <img src="images/icon/fact1.png" alt="" class="img-fluid">
                  </div>
                  <!-- End Fatcs image -->
                  <div class="facts-content wow fadeInUp" data-wow-duration="1s">
                     <h4 class="facts-title">Active clients</h4>
                     <span class="counter">15</span>
                     <sup>+</sup>
                  </div>
                  <!-- Facts Content End -->
               </div>
               <!-- Facts Box End -->
            </div>
            <!-- Col End -->
            <div class="col-md-3 text-center">
               <div class="tw-facts-box">
                  <div class="facts-img wow zoomIn">
                     <img src="images/icon/fact2.png" alt="" class="img-fluid">
                  </div>
                  <!-- End Facts Image -->
                  <div class="facts-content wow slideInUp">
                     <h4 class="facts-title">Projects Done</h4>
                     <span class="counter">28</span>
                     <sup>+</sup>
                  </div>
                  <!-- End Facts Content -->
               </div>
               <!-- End Facts Box -->
            </div>
            <!-- Col End -->
            <div class="col-md-3 text-center">
               <div class="tw-facts-box">
                  <div class="facts-img wow zoomIn">
                     <img src="images/icon/fact3.png" alt="" class="img-fluid">
                  </div>
                  <!-- End Facts Image -->
                  <div class="facts-content wow slideInUp">
                     <h4 class="facts-title">Success Rate</h4>
                     <span class="counter">98</span>
                     <sup>%</sup>
                  </div>
                  <!-- End Facts Content -->
               </div>
               <!-- End Facts Box -->
            </div>
            <!-- Col End -->
            <div class="col-md-3 text-center">
               <div class="tw-facts-box">
                  <div class="facts-img wow zoomIn">
                     <img src="images/icon/fact5.png" alt="" class="img-fluid">
                  </div>
                  <!-- End Facts Image -->
                  <div class="facts-content wow slideInUp">
                     <h4 class="facts-title">Client Satisfaction</h4>
                     <span class="counter">100</span>
                     <sup>%</sup>
                  </div>
                  <!-- End Facts Content -->
               </div>
               <!-- End Facts Box -->
            </div>
            <!-- Col End -->
         </div>
         <!-- Row End -->
      </div>
      <!-- Container End -->
   </section>
   <!-- Facts End -->


   <section id="tw-client" class="tw-client">
      <div class="container">
         <div class="row  wow fadeInUp">
            <div class="col-md-12">
               <div class="clients-carousel owl-carousel">
                  <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="images/clients/client1.png" alt="Paytm">
                     </div>
                     <!-- End Clients logo -->
                  </div>
				   <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="images/clients/client2.png" alt="Navbharat Times">
                     </div>
                     <!-- End Clients logo -->
                  </div>
				   <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="images/clients/client3.png" alt="Adda 52">
                     </div>
                     <!-- End Clients logo -->
                  </div>
				   <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="images/clients/client4.png" alt="Vodafone">
                     </div>
                     <!-- End Clients logo -->
                  </div>
				   <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="images/clients/client5.png" alt="Dream 11">
                     </div>
                     <!-- End Clients logo -->
                  </div>
				   <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="images/clients/client6.png" alt="The Financial Express">
                     </div>
                     <!-- End Clients logo -->
                  </div>
				   <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="images/clients/client7.png" alt="Sony Liv">
                     </div>
                     <!-- End Clients logo -->
                  </div>
				   <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="images/clients/client8.png" alt="Zee5">
                     </div>
                     <!-- End Clients logo -->
                  </div>
				   <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="images/clients/client9.png" alt="Voot">
                     </div>
                     <!-- End Clients logo -->
                  </div>
				   <div class="client-logo-wrapper d-table">
                     <div class="client-logo d-table-cell">
                        <img src="images/clients/client10.png" alt="MX Player">
                     </div>
                     <!-- End Clients logo -->
                  </div>

               </div>
               <!-- End Carousel -->
            </div>
            <!-- End Col -->
         </div>
         <!-- End Row -->
      </div>
      <!-- End Container -->
   </section>
   <!-- End Tw Client -->

</body>




<?php include 'footer.php';?>