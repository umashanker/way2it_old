<?php include 'header.php';?>

<body>


   <div id="banner-area" class="banner-area" style="background-image:url(images/banner/banner5.jpg)">
      <div class="container">
         <div class="row">
            <div class="col-sm-12">
               <div class="banner-heading">
                  <h1 class="banner-title">Terms & Conditions</h1>
                  <ol class="breadcrumb">
                     <li><a href="home">Home</a></li>
                     <li>Terms & Conditions</li>
                  </ol>
               </div>
            </div>
            <!-- Col end -->
         </div>
         <!-- Row end -->
      </div>
      <!-- Container end -->
   </div>
   <!-- Banner area end -->


   <section id="main-container" class="main-container">
         <div class="row">
            <div class="col-lg-12 col-md-12">
               <article class="post tw-news-post">
                     <div class="entry-header">
                        <h2 class="entry-title">
                           Terms
                        </h2>
                        <!-- End Post Meta -->
                     </div>
                     <!-- header end -->
                     <div class="entry-content">
                        <p>By accessing Way2IT websites, you are agreeing to be bound by these Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this web site are protected by applicable copyright and trade mark law.</p>
                     </div>
               </article>
				<article class="post tw-news-post">
                     <div class="entry-header">
                        <h2 class="entry-title">
                           Use License
                        </h2>
                        <!-- End Post Meta -->
                     </div>
                     <!-- header end -->
                     <div class="entry-content">
                        <p>Permission is granted to interact with the materials (information or software) on Way2IT websites for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not:</p>
						 <ul class="list-round">
                           <li>modify or alter;</li>
							 <li>use the materials for any commercial purpose;</li>
							 <li>attempt to decompile or reverse engineer any software contained on Way2IT websites;</li>
							 <li>remove any copyright or other proprietary notations from the materials; or</li>
							 <li>transfer the materials to another person or “mirror” the materials on any other server.</li>
                        </ul>
						 <p>This license shall automatically terminate if you violate any of these restrictions and may be terminated by Way2IT at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.</p>
                     </div>
               </article>
				<article class="post tw-news-post">
                     <div class="entry-header">
                        <h2 class="entry-title">
                           Disclaimer
                        </h2>
                        <!-- End Post Meta -->
                     </div>
                     <!-- header end -->
                     <div class="entry-content">
                        <p>The materials on our websites are provided “as is”. Way2IT makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, Way2IT does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet web site or otherwise relating to such materials or on any sites linked to this site.</p>
                     </div>
               </article>
				<article class="post tw-news-post">
                     <div class="entry-header">
                        <h2 class="entry-title">
                           Limitations
                        </h2>
                        <!-- End Post Meta -->
                     </div>
                     <!-- header end -->
                     <div class="entry-content">
                        <p>In no event shall Way2IT or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption,) arising out of the use or inability to use the materials on Way2IT websites, even if Way2IT or a Way2IT authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.</p>
                     </div>
               </article>
				<article class="post tw-news-post">
                     <div class="entry-header">
                        <h2 class="entry-title">
                           Revisions and Errata
                        </h2>
                        <!-- End Post Meta -->
                     </div>
                     <!-- header end -->
                     <div class="entry-content">
                        <p>Although Way2IT strives for accuracy, the materials appearing on Way2IT websites could include technical, typographical, or photographic errors. Way2IT does not warrant that any of the materials on its websites are accurate, complete, or current. Way2IT may make changes to the materials contained on its websites at any time without notice. Way2IT does not, however, make any commitment to update the materials.</p>
                     </div>
               </article>
				<article class="post tw-news-post">
                     <div class="entry-header">
                        <h2 class="entry-title">
                           Links
                        </h2>
                        <!-- End Post Meta -->
                     </div>
                     <!-- header end -->
                     <div class="entry-content">
                        <p>Way2IT has not reviewed all of the sites linked to its Internet websites and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Way2IT of the site. Use of any such linked website is at the user’s own risk.</p>
                     </div>
               </article>
				<article class="post tw-news-post">
                     <div class="entry-header">
                        <h2 class="entry-title">
                           Governing Law
                        </h2>
                        <!-- End Post Meta -->
                     </div>
                     <!-- header end -->
                     <div class="entry-content">
                        <p>Any claim relating to Way2IT websites shall be governed by the laws of the State of Colorado without regard to its conflict of law provisions.</p>
                     </div>
               </article>
				<article class="post tw-news-post">
                     <div class="entry-header">
                        <h2 class="entry-title">
                           Content Standards
                        </h2>
                        <!-- End Post Meta -->
                     </div>
                     <!-- header end -->
                     <div class="entry-content">
                        <p>These content standards apply to all user contributions i.e. usernames or comments and use of interactive services, if offered. User contributions must in their entirety comply with all applicable local and international laws and regulations. Without limiting the foregoing, user contributions must not:</p>
						 <ul class="list-round">
						 <li>Promote sexually explicit or pornographic material, violence, or discrimination based on race, sex, religion, nationality, disability, sexual orientation or age</li>
							 <li>Infringe any patent, trademark, trade secret, copyright or other intellectual property or other rights of any third-party</li>
							 <li>Infringe the legal rights (including the right of publicity and privacy) of others or contain any material that could give rise to any civil or criminal liability under applicable laws</li>
							 <li>Promote any illegal activity, or advocate, promote or assist any unlawful act</li>
							 <li>Impersonate any person, or misrepresent your identity or affiliation with any person or organization</li>
							 <li>Involve commercial activities or sales</li>
							 <li>Be likely to deceive or give the impression that they emanate from or are endorsed by Way2IT, or any other person or entity</li>
						 
						 
						 </ul>
                     </div>
               </article>
				<article class="post tw-news-post">
                     <div class="entry-header">
                        <h2 class="entry-title">
                           Monitoring and Enforcement; Termination
                        </h2>
                        <!-- End Post Meta -->
                     </div>
                     <!-- header end -->
                     <div class="entry-content">
                        <p>We have the right, but not the obligation, to review, screen or edit any user contribution. You accept that such contributions do not reflect the views of Way2IT and are not endorsed by Way2IT.</p>
						 <p>We have the right to: (a) remove or refuse to post any user contributions for any reason; (b) take any action with respect to user contributions that we deem necessary or appropriate; (c) disclose your identity or other information about you to any third-party who in our opinion reasonably claims that material posted by you infringes their rights, including their intellectual property rights or their right to privacy; (d) take appropriate legal action, including without limitation, referral to law enforcement, for any illegal or unauthorized use of our websites; (e) terminate or suspend your access to all or part of our websites.</p>
						 <p>Without limiting the foregoing, we have the right to fully cooperate with law enforcement authorities requesting or directing us to disclose the identity or other information of anyone posting any materials on or through our websites. We do not undertake to review material before it is posted on our websites, and cannot ensure prompt removal of objectionable material after it has been posted. Accordingly, we assume no liability for any action or inaction regarding transmissions, communications or content provided by any user or third-party. We have no liability or responsibility to anyone for performance or nonperformance of the activities described in this section.</p>
                     </div>
               </article>
				<article class="post tw-news-post">
                     <div class="entry-header">
                        <h2 class="entry-title">
                           User Password and Security
                        </h2>
                        <!-- End Post Meta -->
                     </div>
                     <!-- header end -->
                     <div class="entry-content">
                        <p>If you use a password to access our websites or any portion of them, then you are responsible for maintaining the confidentiality of the password and for restricting access to your computer, and you agree to accept responsibility for all activities that occur on your account or with your password. In the event that the confidentiality of your account or password is compromised in any manner, you will notify Way2IT immediately. Way2IT reserves the right to take any and all action, as it deems necessary or reasonable to maintain the security of our websites and your account, including without limitation, terminating your account, changing your, password or requesting information to authorize transactions on your account. While Way2IT takes prudent steps to protect your account and its websites, it cannot protect your information outside of our websites.</p>
                     </div>
               </article>
				<article class="post tw-news-post">
                     <div class="entry-header">
                        <h2 class="entry-title">
                           Site Terms and Conditions of use Modifications
                        </h2>
                        <!-- End Post Meta -->
                     </div>
                     <!-- header end -->
                     <div class="entry-content">
                        <p>Way2IT may revise these Terms and Conditions of Use for its websites at any time without notice. By using these websites you are agreeing to be bound by the then current version of these Terms and Conditions of Use.</p>
                     </div>
               </article>
				<article class="post tw-news-post">
                     <div class="entry-header">
                        <h2 class="entry-title">
                           Third Party Analytics and Advertising
                        </h2>
                        <!-- End Post Meta -->
                     </div>
                     <!-- header end -->
                     <div class="entry-content">
						 <h4>Facebook</h4>
                        <p>Facebook Connect. For more information about what Facebook collects when you use Facebook buttons on Infront Webworks, please see: Data Policy.<br>Facebook Ads. For more information about what Facebook collects and your choices around Facebook Ads, please see: AdChoices.</p>
						 <h4>Twitter</h4>
                        <p>For more information about what Twitter collects when you use Twitter buttons on Infront Webworks or visit pages on Infront Webworks that include these buttons, please see: <a href="https://twitter.com/en/privacy">Privacy</a>.</p>
						 <h4>Linkedin</h4>
                        <p>For more information about what LinkedIn collects when you use LinkedIn buttons on Infront Webworks or visit pages on Infront Webworks that include these buttons, please see <a href="https://www.linkedin.com/legal/privacy-policy">Privacy Policy</a> | <a href="https://www.linkedin.com/legal/privacy-policy">LinkedIn</a></p>
						 <h4>Google Analytics</h4>
                        <p>For more information about Google Analytics cookies, please see Google's help pages and privacy policy: <a href="https://policies.google.com/privacy">Google's Privacy Policy</a>
<a href="https://support.google.com/analytics/answer/6004245?hl=en">Google Analytics Help pages</a>

Google has developed the Google Analytics opt-out browser add-on; if you want to opt out of Google Analytics, you can download and install the add-on for your web browser <a href="https://tools.google.com/dlpage/gaoptout">here</a>.

</p>
                     </div>
               </article>
				<article class="post tw-news-post">
                     <div class="entry-header">
                        <h2 class="entry-title">
                           More Information About Cookies
                        </h2>
                        <!-- End Post Meta -->
                     </div>
                     <!-- header end -->
                     <div class="entry-content">
                        <p>International Chamber of Commerce United Kingdom<br>
							Information on the ICC (UK) UK cookie guide can be found on the ICC website: International Chamber of Commerce UK - Digital Economy.<br>
							Changes to these Terms were last made on May 24, 2018.<br>
							If you feel that Way2IT or some other user is not abiding by this Terms and Conditions of Use, contact us immediately via telephone +91-7838257939 or our Contact Us page.
						 </p>
						 
                     </div>
               </article>
            </div>
            <!-- Content Col end -->
         </div>
         <!-- Main row end -->
   </section>


</body>
<?php include 'footer.php';?>