<body>
   
   <footer id="tw-footer" class="tw-footer">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-lg-4">
               <div class="tw-footer-info-box">
                  <a href="home" class="footer-logo">
                     <img src="images/logo/logo-white.png" alt="Pink Pixels" width="150" height="150" class="img-fluid">
                  </a>
                  <p class="footer-info-text">
                     Way2IT Techno Services Private Limited
                  </p>
                  <div class="footer-social-link">
                     <h3>Follow us</h3>
                     <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                     </ul>
                  </div>
                  <!-- End Social link -->
               </div>
               <!-- End Footer info -->
            </div>
            <!-- End Col -->
            <div class="col-md-12 col-lg-8">
               <div class="row">
                  <div class="col-md-6">
                     <div class="contact-us">
                        <div class="contact-icon">
                           <i class="icon icon-map2"></i>
                        </div>
                        <!-- End contact Icon -->
                        <div class="contact-info">
                           <p>B-48, First Floor Sector 63, Noida, Uttar Pradesh 201301</p>
                        </div>
                        <!-- End Contact Info -->
                     </div>
                     <!-- End Contact Us -->
                  </div>
                  <!-- End Col -->
                  <div class="col-md-6">
                     <div class="contact-us contact-us-last">
                        <div class="contact-icon">
                           <i class="icon icon-phone3"></i>
                        </div>
                        <!-- End contact Icon -->
                        <div class="contact-info">
                           <h3>+91-7838257939<br>+91-8837813622</h3>
                           <p>Give us a call</p>
                        </div>
                        <!-- End Contact Info -->
                     </div>
                     <!-- End Contact Us -->
                  </div>
                  <!-- End Col -->
               </div>
               <!-- End Contact Row -->
               <div class="row">
                  <div class="col-md-12 col-lg-6">
                     <div class="footer-widget footer-left-widget">
                        <div class="section-heading">
                           <h3>Useful Links</h3>
                           <span class="animate-border border-black"></span>
                        </div>
                        <ul>
                           <li><a href="about">About us</a></li>
                           <li><a href="contact">Contact us</a></li>
                        </ul>
                        <ul>
                           <li><a href="#">Testimonials</a></li>
                           <li><a href="faq">Faq</a></li>
                        </ul>
                     </div>
                     <!-- End Footer Widget -->
                  </div>
                  <!-- End col -->
                  <div class="col-md-12 col-lg-6">
                     <div class="footer-widget">
                        <div class="section-heading">
                           <h3>Subscribe</h3>
                           <span class="animate-border border-black"></span>
                        </div>
                        <p>Don’t miss to subscribe to our new feeds, kindly fill the form below.</p>
                        <form action="#">
                           <div class="form-row">
                              <div class="col tw-footer-form">
                                 <input type="email" class="form-control" placeholder="Email Address">
                                 <button type="submit"><i class="fa fa-send"></i></button>
                              </div>
                           </div>
                        </form>
                        <!-- End form -->
                     </div>
                     <!-- End footer widget -->
                  </div>
                  <!-- End Col -->
               </div>
               <!-- End Row -->
            </div>
            <!-- End Col -->
         </div>
         <!-- End Widget Row -->
      </div>
      <!-- End Contact Container -->


      <div class="copyright">
         <div class="container">
            <div class="row">
               <div class="col-md-6">
                  <span>Copyright &copy; 2019, All Right Reserved Way2IT</span>
               </div>
               <!-- End Col -->
               <div class="col-md-6">
                  <div class="copyright-menu">
                     <ul>
                        <li><a href="home">Home</a></li>
                        <li><a href="terms">Terms & Conditions</a></li>
                        <li><a href="contact">Contact</a></li>
                     </ul>
                  </div>
               </div>
               <!-- End col -->
            </div>
            <!-- End Row -->
         </div>
         <!-- End Copyright Container -->
      </div>
      <!-- End Copyright -->
      <!-- Back to top -->
      <div id="back-to-top" class="back-to-top">
         <button class="btn btn-dark" title="Back to Top">
            <i class="fa fa-angle-up"></i>
         </button>
      </div>
      <!-- End Back to top -->
   </footer>
   <!-- End Footer -->
</body>
</html>