<?php 
$url = basename($_SERVER['PHP_SELF']);

?>

<!doctype html>
<html lang="en">

<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154628343-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-154628343-1');
</script>


   <!-- Basic Page Needs =====================================-->
   <meta charset="utf-8">

   <!-- Mobile Specific Metas ================================-->
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

   <!-- Site Title- -->
   <title>Way2IT - Website design, Software Development, Mobile Application</title>
	
	<!-- Favicon -->
    <link rel="icon" href="images/logo/favicon.ico">

   <!-- CSS
   ==================================================== -->
   <!--Font Awesome -->
   <link rel="stylesheet" href="css/font-awesome.min.css" />

   <!-- Animate CSS -->
   <link rel="stylesheet" href="css/animate.css">

   <!-- Iconic Fonts -->
   <link rel="stylesheet" href="css/icofonts.css" />

   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="css/bootstrap.min.css">

   <!-- Owl Carousel -->
   <link rel="stylesheet" href="css/owlcarousel.min.css" />

   <!-- Video Popup -->
   <link rel="stylesheet" href="css/magnific-popup.css" />

   <!--Style CSS -->
   <link rel="stylesheet" href="css/style.css">

   <!--Responsive CSS -->
   <link rel="stylesheet" href="css/responsive.css">


   <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->


</head>

<body>
   


   <div class="tw-top-bar">
      <div class="container">
         <div class="row">
            <div class="col-md-8 text-left">
               <div class="top-contact-info">
                  
                  <span><i class="icon icon-envelope"></i>support@way2it.in</span>
                  <span><i class="icon icon-phone3"></i>+91-7838257939, +91-8837813622</span>
               </div>
            </div>
            <!-- Col End -->
            <div class="col-md-4 ml-auto text-right">
               <div class="top-social-links">
                  <span>Follow us:</span>
                  <a href="#"><i class="fa fa-facebook"></i></a>
                  <a href="#"><i class="fa fa-google-plus"></i></a>
               </div>
            </div>
            <!-- Col End -->
         </div>
         <!-- Row End -->
      </div>
      <!-- Container End -->
   </div>
   <!-- Top Bar end -->


   <!-- header======================-->
   <header>
      <div class="tw-head">
         <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-white">
               <a class="navbar-brand tw-nav-brand" href="home">
                  <img src="images/logo/logo.png" width="80" height="80" alt="Way2it">
               </a>
               <!-- End of Navbar Brand -->
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                  aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
               </button>
               <!-- End of Navbar toggler -->
               <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
                  <ul class="navbar-nav">
	<li class="nav-item <?php if($url == 'home.php'){?>active<?php } ?>"><a class="nav-link" href="home">Home</a></li>
					  <li class="nav-item <?php if($url == 'about.php'){?>active<?php } ?>"><a class="nav-link" href="about">About</a></li>
                     
   <li class="nav-item dropdown <?php if($url == 'web-design.php' || $url == 'web-development.php' || $url == 'software-development.php' || $url == 'app-development.php' || $url == 'seo-services.php' || $url == 'social-media-marketing.php' || $url == 'pay-per-click.php' || $url == 'e-mail-marketing.php'){?>active<?php } ?>">
                        <a class="nav-link" href="#" data-toggle="dropdown">
                           Services
                           <span class="tw-indicator"><i class="fa fa-angle-down"></i></span>
                        </a>
                        <ul class="dropdown-menu tw-dropdown-menu">
                           <li><a href="web-design">Website Design</a></li>
                           <li><a href="web-development">Web Development</a></li>
                           <li><a href="software-development">Software Development</a></li>
                           <li><a href="app-development">App Development</a></li>
                           <li><a href="seo-services">SEO Services</a></li>
                           <li><a href="social-media-marketing">SMO Services</a></li>
                           <li><a href="pay-per-click">PPC Advertising</a></li>
							<li><a href="e-mail-marketing">E-mail Marketing</a></li>
                        </ul>
                        <!-- End of Dropdown menu -->
                     </li>
                     <!-- End Dropdown -->
					  <li class="nav-item"><a class="nav-link" href="#">Portfolio</a></li>
					  
                     <!-- 
                     <li class="nav-item dropdown tw-megamenu-wrapper">
                        <a class="nav-link" href="#" data-toggle="dropdown">
                           Features
                           <span class="tw-indicator">
                              <i class="fa fa-angle-down"></i>
                           </span>
                        </a>
                        <div id="tw-megamenu" class="dropdown-menu tw-mega-menu">
                           <div class="row">
                              <div class="col-md-12 col-lg-3 no-padding">
                                 <ul>
                                    <li class="tw-megamenu-title">
                                       <h3>Standard Pricing</h3>
                                    </li>
                                    <li><a href="pricing.html">SEO Pricing</a></li>
                                    <li><a href="#">Ecommerce SEO</a></li>
                                    <li><a href="#">Enterprise SEO</a></li>
                                    <li><a href="#">Local SEO</a></li>
                                    <li><a href="#">SEO Audits</a></li>
                                    <li><a href="#">PPC Management</a></li>
                                    <li><a href="#">SEO Link Buildings</a></li>
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-12 col-lg-3 no-padding">
                                 <ul>
                                    <li class="tw-megamenu-title">
                                       <h3>Elements One</h3>
                                    </li>
                                    <li><a href="pricing.html">Addon list 1</a></li>
                                    <li><a href="#">Addon list 2</a></li>
                                    <li><a href="#">Addon list 3</a></li>
                                    <li><a href="#">Addon list 4</a></li>
                                    <li><a href="#">Addon list 5</a></li>
                                    <li><a href="#">Addon list 6</a></li>
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-12 col-lg-3 no-padding">
                                 <ul>
                                    <li class="tw-megamenu-title">
                                       <h3>Elements Two</h3>
                                    </li>
                                    <li><a href="#">Addon list 7</a></li>
                                    <li><a href="#">Addon list 8</a></li>
                                    <li><a href="#">Addon list 9</a></li>
                                    <li><a href="#">Addon list 10</a></li>
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-12 col-lg-3 no-padding">
                                 <ul>
                                    <li class="tw-megamenu-title">
                                       <h3>Site review</h3>
                                    </li>
                                    <li>
                                       <p>Start working with the best company that provide you everything you need.</p>
                                    </li>
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </li>
                     -->
					  
 
<!--                     <li class="nav-item"><a class="nav-link" href="contact">Contact</a></li>-->
					  
					  <li class="nav-item <?php if($url == 'contact.php'){?>active<?php } ?>"><a class="nav-link" href="contact">Contact</a></li>
					  
					  
                  </ul>
                  <!-- End Navbar Nav -->
               </div>
               <!-- End of navbar collapse -->
            </nav>
            <!-- End of Nav -->
         </div>
         <!-- End of Container -->
      </div>
      <!-- End tw head -->
   </header>
   <!-- End of Header area=-->


  <!-- Javascripts File
      =============================================================================-->

   <!-- initialize jQuery Library -->
   <script src="js/jquery-2.0.0.min.js"></script>
   <!-- Popper JS -->
   <script src="js/popper.min.js"></script>
   <!-- Bootstrap jQuery -->
   <script src="js/bootstrap.min.js"></script>
   <!-- Owl Carousel -->
   <script src="js/owl-carousel.2.3.0.min.js"></script>
   <!-- Waypoint -->
   <script src="js/waypoints.min.js"></script>
   <!-- Counter Up -->
   <script src="js/jquery.counterup.min.js"></script>
   <!-- Video Popup -->
   <script src="js/jquery.magnific.popup.js"></script>
   <!-- Smooth scroll -->
   <script src="js/smoothscroll.js"></script>
   <!-- WoW js -->
   <script src="js/wow.min.js"></script>
   <!-- Template Custom -->
   <script src="js/main.js"></script>
	
<!--
	<script>
// Add active class to the current button (highlight it)
var header = document.getElementById("activefnc");
var btns = header.getElementsByClassName("nbc");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("active");
  current[0].className = current[0].className.replace(" active", "");
  this.className += " active";
  });
}
</script>
-->

</body>


</html>
