<?php include 'header.php';?>

<body>


   <div id="banner-area" class="banner-area" style="background-image:url(images/banner/banner5.jpg)">
      <div class="container">
         <div class="row">
            <div class="col-sm-12">
               <div class="banner-heading">
                  <h1 class="banner-title">E-mail Marketing</h1>
                  <ol class="breadcrumb">
                     <li><a href="home">Home</a></li>
                     <li>E-mail Marketing</li>
                  </ol>
               </div>
            </div>
            <!-- Col end -->
         </div>
         <!-- Row end -->
      </div>
      <!-- Container end -->
   </div>
   <!-- Banner area end -->


   <section id="main-container" class="main-container">
      <div class="container">
       	  <div class="row">
            <div class="col-md-4">
               <img src="images/email/email.png" alt="email marketing" class="img-fluid">
            </div>
            <!-- Col End -->
            <div class="col-md-8">
               <div class="tw-about-bin">
                  <h2 class="column-title">Email Marketing Made Easy</h2>
				   <p><b>You’ve got mail</b></p>
                  <span class="animate-border tw-mb-30"></span>
                  <p>There’s something to be said about an email that catches your eye. The subject, the offer, the design, or even the timing — all of these things come together to craft a perfect message to pierce through the noise when done correctly.
				   <br><br>
				   It’s touchpoints like these that contribute to an effective sales and marketing strategy. A well segmented, yet automated, email can provide the type of personalized experience today’s consumer has grown accustomed to.
					  <br><br>
					  It’s that type of personalization that allows email marketing to maintain its effectiveness in the face of so many “newer” platforms.
				   </p>
				   <br>
					<h4>Don’t be Spam</h4>
				   <p>Email marketing is the most personalized way you can connect with your audience. Go straight to your customers’ inboxes, and meet them where they already are! Youtech helps our clients develop and execute personalized email marketing campaigns to drive traffic and promote conversions. No more hiding in the spam folders! Our talented design and content gurus will grab the attention of new and old customers to deliver an advanced email marketing campaign for your company.</p>
				   
				   
				   <div class="row">
				   <div class="col-lg-6 col-md-12">
				   <h4>Business Email</h4>
				   <ul class="list-round">
				   		<li>Campaign Setup</li>
					    <li>1 Email Template</li>
					   <li>Account Creation/Migration</li>
					   <li>Email Capture as a Form on Client Website</li>
					   <li>Monthly Email</li>
					   <li>1 Email Sent Through Platform</li>
					   
				   
				   </ul>
					   </div>
				   <div class="col-lg-6 col-md-12">
				   <h4>Drip Email</h4>
				   <ul class="list-round">
				   		<li>Campaign Setup</li>
					    <li>Drip Workflow</li>
					   <li>List Segmentation</li>
					   <li>Drip Email Creation (up to 10 in sequence)</li>
					   <li>Account Creation/Migration</li>
					   <li>Email Capture as a Form on Client Website</li>
				   
				   </ul>
					   
					  
					   
					   </div>
					   </div>
				   
				   <h4>Privacy</h4>
				   <p>Needless to say, you can’t just add someone to an email list and blast away. Our email marketing experts are well versed in both the CAN-SPAM Act and GDPR requirements to ensure you aren’t breaking any laws when collecting information or sending out email blasts.</p>
					   
					   </div>
               </div>
               <!-- About PinkPixels End -->
            </div>
            <!-- Col End -->
			 
			  
            <!-- Col End -->
		  </div>  
         

		  
      </div>
      <!-- Container end -->
   </section>


</body>
<?php include 'footer.php';?>