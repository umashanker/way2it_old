<?php include 'header.php';?>

<body>


   <div id="banner-area" class="banner-area" style="background-image:url(images/banner/banner5.jpg)">
      <div class="container">
         <div class="row">
            <div class="col-sm-12">
               <div class="banner-heading">
                  <h1 class="banner-title">About us</h1>
                  <ol class="breadcrumb">
                     <li><a href="home">Home</a></li>
                     <li>About us</li>
                  </ol>
               </div>
            </div>
            <!-- Col end -->
         </div>
         <!-- Row end -->
      </div>
      <!-- Container end -->
   </div>
   <!-- Banner area end -->


   <section id="main-container" class="main-container">
      <div class="container">
         <div class="row">
            <div class="col-md-6 align-self-md-center">
               <img src="images/about/about.png" alt="about" class="img-fluid">
            </div>
            <!-- Col End -->
            <div class="col-md-6">
               <div class="tw-about-bin">
                  <h2 class="column-title">About Way2IT</h2>
                  <span class="animate-border tw-mb-40 tw-mt-20"></span>
				  
                  <p><b>Right from HTML to .NET, from the traditional Pay-Per-Click Marketing to the new age Social Media Optimization, and from Website Designing to Optimisation of the Site for various devices, we ensure that our clients can seamlessly rely on us for all their needs in the context of establishing the presence of their business in the online arena.</b></p>
                  <p>Way2IT is a talented bunch of bonkers experts well versed with web design, development and digital marketing services. The company was established in the year 2019. It was only a matter of a few months that a commendable number of experienced industry experts joined in this small team, and Way2IT became an invincible organization, not just with respect to the insurmountable knowledge, but also in terms of commendable experience.</p>
               </div>
               <!-- About Way2IT End -->
            </div>
            <!-- Col End -->
         </div>
         <!-- Row end -->
      </div>
      <!-- Container end -->
   </section>
   <!-- Main container end -->
	
   <section id="tw-mission" class="tw-mission">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="mission-carousel owl-carousel">
                  <div class="row">
                     <div class="col-md-6 mr-auto align-self-md-center">
                        <img src="images/about/our_mission.png" alt="" class="img-fluid">
                     </div>
                     <!-- Col End -->
                     <div class="col-md-6">
                        <div class="mission-body">
                           <div class="mission-title tw-mb-40">
                              <h2 class="column-title">What Drives Us?</h2>
                              <span class="animate-border bg-white border-orange tw-mt-30"></span>
                           </div>
                           <p>
                            Founded with the vision of helping every business earn an online presence, the company has always emphasized on getting the work done no matter what the budget of the client is.
							It is this passionate drive that helps the company to tread forward, even in today’s times of extreme competition. For what it’s worth, all our clients have found complete satisfaction in our services.
                           </p>
                        </div>
                     </div>
                     <!-- Col End -->
                  </div>
               </div>
               <!-- Mission Carousel End -->
            </div>
            <!-- Col End -->
         </div>
         <!-- Row End -->
      </div>
      <!-- Container End -->
   </section>
   <!-- Mission End -->
	
		 <section id="main-container" class="main-container">
      <div class="container">
         <div class="row">
            <div class="col-lg-3 col-md-12">
               <div class="section-heading">
                  <h2>
                     What You <span>Get?</span>
                  </h2>
                  <span class="animate-border tw-mt-30 tw-mb-40"></span>
                  <p>Thanks to our impeccable team of dedicated experts, you can expect –</p>
               </div>
            </div>
            <!-- Heading Col End -->
            <div class="col-lg-9 col-md-12">
               <div class="service-list-carousel owl-carousel">
                  <div class="tw-service-box-list text-center">
                     <div class="service-list-bg service-list-bg-1 d-table">
                        <div class="service-list-icon d-table-cell">
                           <img src="images/icon/service1.png" alt="" class="img-fluid">
                        </div>
                     </div>
                     <!-- List Bg End -->
                     <h3>An attractive website that functions seamlessly</h3>
                  </div>
                  <!-- List Box End -->
                  <div class="tw-service-box-list text-center">
                     <div class="service-list-bg service-list-bg-2 d-table">
                        <div class="service-list-icon d-table-cell">
                           <img src="images/icon/service2.png" alt="" class="img-fluid">
                        </div>
                     </div>
                     <!-- List Bg End -->
                     <h3>User-<br>
friendly <br>
designs</h3>
                  </div>
                  <!-- List Box End -->
                  <div class="tw-service-box-list text-center">
                     <div class="service-list-bg service-list-bg-3 d-table">
                        <div class="service-list-icon d-table-cell">
                           <img src="images/icon/service3.png" alt="" class="img-fluid">
                        </div>
                     </div>
                     <!-- List Bg End -->
                     <h3>Optimized web designs that work flawlessly on all devices</h3>
                  </div>
                  <!-- List Box End -->
                  <div class="tw-service-box-list text-center">
                     <div class="service-list-bg service-list-bg-1 d-table">
                        <div class="service-list-icon d-table-cell">
                           <img src="images/icon/service1.png" alt="" class="img-fluid">
                        </div>
                     </div>
                     <!-- List Bg End -->
                     <h3>Result <br>
oriented <br>
web pages</h3>
                  </div>
                  <!-- List Box End -->
				   <div class="tw-service-box-list text-center">
                     <div class="service-list-bg service-list-bg-1 d-table">
                        <div class="service-list-icon d-table-cell">
                           <img src="images/icon/service1.png" alt="" class="img-fluid">
                        </div>
                     </div>
                     <!-- List Bg End -->
                     <h3>Search Engine and Social Media Optimization</h3>
                  </div>
                  <!-- List Box End -->
				   
               </div>
               <!-- Carousel End -->
            </div>
            <!-- Content Col end -->
         </div>
         <!-- Row End -->
      </div>
      <!-- Container End -->
   </section>
   <!-- Service List End -->
	
	  <section id="tw-case-working-process" class="tw-case-working-process bg-offwhite">
      <div class="container">
         <div class="row">
            <div class="col text-center">
               <div class="section-heading">
                  <h2>
                     <small>At Way2IT, we take it as our onus to bring every business online, no matter how small it is.
</small>
                     Why <span>Us?</span>
                  </h2>
                  <span class="animate-border ml-auto mr-auto tw-mt-20 tw-mb-40"></span>
				   <p>We aim to conceptualize, create and maintain a unique identity for every brand, so that it gains unprecedented recognition, and eventually succeeds in making the profits that it intends to! However, what makes us truly unique is the fact that our design specialists, as well as marketing strategists, can help design an optimal end-to-end solution for your business well within your budgetary constraints.

</p>
               </div>
            </div>
            <!-- Col End -->
         </div>
         <!-- Title Row End -->
      </div>
      <!-- Container End -->
   </section>
   <!-- Working Process End -->


	<section id="tw-call-to-action" class="tw-call-to-action">
      <div class="container">
         <div class="call-action-bg-pattern">
            <img src="images/services/call_to%20_action_img1.png" alt="">
            <img src="images/services/call_to%20_action_img2.png" alt="">
         </div>
         <!-- Bg Pattern End -->
         <div class="row">
            <div class="col text-center">
               <h2 class="column-title">
                  <small>Promise to take you on top</small>
                  Take your website to TOP of <span>Search Engines</span>
               </h2>
               <a href="tel:+917838257939" class="btn btn-primary tw-mt-20 btn-lg">call us</a>
            </div>
            <!-- Col End -->
         </div>
         <!-- Row End -->
      </div>
      <!-- Container End -->
   </section>



</body>
<?php include 'footer.php';?>