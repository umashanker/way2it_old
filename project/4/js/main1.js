(function($) {

    "use strict";

    $.fn.hasAttr = function(attr) {  
       if (typeof attr !== typeof undefined && attr !== false && attr !== undefined) {
            return true;
       }
       return false;
    };

    /*-------------------------------------
     Background Image Function
    -------------------------------------*/
    var background_image = function() {
        $("[data-bg-img]").each(function() {
            var attr = $(this).attr('data-bg-img');
            if (typeof attr !== typeof undefined && attr !== false && attr !== "") {
                $(this).css('background-image', 'url('+attr+')');
            }
        });  
    };

    /*-------------------------------------
     Background Color Function
    -------------------------------------*/
    var background_color = function() {
        $("[data-bg-color]").each(function() {
            var attr = $(this).attr('data-bg-color');
            if (typeof attr !== typeof undefined && attr !== false && attr !== "") {
                $(this).css('background-color', attr);
            }
        });  
    };

    var link_void = function() {
        $("a[data-prevent='default']").each(function() {
            $(this).on('click', function(e) {
                e.preventDefault();
            });
        });
    };

    /*-------------------------------------
     Preloader
    -------------------------------------*/
    var preloader = function() {
        if($('#preloader').length) {
            $('#preloader > *').fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(150).fadeOut('slow'); // will fade out the white DIV that covers the website.
            $('body').delay(150).removeClass('preloader-active');
        }
    };

    /*-------------------------------------
     HTML attr direction
    -------------------------------------*/
    var html_direction = function() {
        var html_tag = $("html"),
            dir = html_tag.attr("dir"),
            directions = ['ltr', 'rtl'];
        if (html_tag.hasAttr('dir') && jQuery.inArray(dir, directions)) {
            html_tag.addClass(dir);
        } else {
            html_tag.attr("dir", directions[0]).addClass(directions[0]);
        }
    };
    

    /*-------------------------------------
     CSS fix for IE Mobile
    -------------------------------------*/
    var bugfix = function() {
        if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
          var msViewportStyle = document.createElement('style');
          msViewportStyle.appendChild(
            document.createTextNode(
              '@-ms-viewport{width:auto!important}'
            )
          );
          document.querySelector('head').appendChild(msViewportStyle);
        }
    };

    /*-------------------------------------
     Toggle Class function
    -------------------------------------*/
    var toogle_class = function() {
        $('[data-toggle-class]').each(function(){
            var current = $(this),
                toggle_event = current.data('toggle-event'),
                toggle_class = current.data('toggle-class');

            if (toggle_event == "hover") {
                current.on("mouseenter", function() {
                    if (current.hasClass(toggle_class) === false) {
                        $(this).addClass(toggle_class);
                    }
                });
                current.on("mouseleave", function() {
                    if (current.hasClass(toggle_class) === true) {
                        $(this).removeClass(toggle_class);
                    }
                });
            }
            current.on(toggle_event, function() {
                $(this).toggleClass(toggle_class);
            });
        });
    };


    /*-------------------------------------
     Back Top functions
    -------------------------------------*/
    var back_to_top = function() {
        var backTop = $('#backTop');
        if (backTop.length) {
            var scrollTrigger = 200,
                scrollTop = $(window).scrollTop();
            if (scrollTop > scrollTrigger) {
                backTop.addClass('show');
            } else {
                backTop.removeClass('show');
            }
        }
    };
    var click_back = function() {
        var backTop = $('#backTop');
        backTop.on('click', function(e) {
            $('html,body').animate({
                scrollTop: 0
            }, 700);
            e.preventDefault();
        });
    };

    /*-------------------------------------
     Navbar Functions
    -------------------------------------*/
    var navbar_js = function() {
        $('.dropdown-mega-menu > a, .nav-menu > li:has( > ul) > a').append("<span class=\"indicator\"><i class=\"fa fa-angle-down\"></i></span>");
        $('.nav-menu > li ul > li:has( > ul) > a').append("<span class=\"indicator\"><i class=\"fa fa-angle-right\"></i></span>");
        $(".dropdown-mega-menu, .nav-menu li:has( > ul)").on('mouseenter', function () {
            if ($(window).width() > 943) {
                $(this).children("ul, .mega-menu").fadeIn(100);
            }
        });
        $(".dropdown-mega-menu, .nav-menu li:has( > ul)").on('mouseleave', function () {
            if ($(window).width() > 943) {
                $(this).children("ul, .mega-menu").fadeOut(100);
            }
        });
        $(".dropdown-mega-menu > a, .nav-menu li:has( > ul) > a").on('click', function (e) {
            if ($(window).width() <= 943) {
                $(this).parent().addClass("active-mobile").children("ul, .mega-menu").slideToggle(150, function() {
                    
                });
                $(this).parent().siblings().removeClass("active-mobile").children("ul, .mega-menu").slideUp(150);
            }
            e.preventDefault();
        });
        $(".nav-toggle").on('click', function (e) {
            var toggleId = $(this).data("toggle");
            $(toggleId).slideToggle(150);
            e.preventDefault();
        });
    };
	
	
    var navbar_resize_load = function() {
        if ($(".nav-header").css("display") == "block") {
            $(".nav-bar").addClass('nav-mobile');
            $('.nav-menu').find("li.active").addClass("active-mobile");
        }
        else {
            $(".nav-bar").removeClass('nav-mobile');
        }

        if ($(window).width() >= 943) {
            $(".dropdown-mega-menu a, .nav-menu li:has( > ul) a").each(function () {
                $(this).parent().children("ul, .mega-menu").slideUp(0);
            });
            $($(".nav-toggle").data("toggle")).show();
            $('.nav-menu').find("li").removeClass("active-mobile");
        }
    };

    /*-------------------------------------
     Social Icons Share
    -------------------------------------*/
    var share_social = function() {
        var share_action = $('.deal-actions .share-btn');
        share_action.on('click',function(){
            var share_icons = $(this).children('.share-tooltip');
            share_icons.toggleClass('in');
        });
    };

    /*-------------------------------------
     Add Deal to Favorite
    -------------------------------------*/
    var add_favorite = function() {
        var like_btn = $('.actions .like-deal');
        like_btn.on('click',function(){
            $(this).toggleClass('favorite');
        });
    };

    /*-------------------------------------
     Carousel slider initiation
    -------------------------------------*/
    var owl_carousel = function() {
        $('.owl-slider').each(function () {
            var carousel = $(this),
                autoplay_hover_pause = carousel.data('autoplay-hover-pause'),
                loop = carousel.data('loop'),
                items_general = carousel.data('items'),
                margin = carousel.data('margin'),
                autoplay = carousel.data('autoplay'),
                autoplayTimeout = carousel.data('autoplay-timeout'),
                smartSpeed = carousel.data('smart-speed'),
                nav_general = carousel.data('nav'),
                navSpeed = carousel.data('nav-speed'),
                xxs_items = carousel.data('xxs-items'),
                xxs_nav = carousel.data('xxs-nav'),
                xs_items = carousel.data('xs-items'),
                xs_nav = carousel.data('xs-nav'),
                sm_items = carousel.data('sm-items'),
                sm_nav = carousel.data('sm-nav'),
                md_items = carousel.data('md-items'),
                md_nav = carousel.data('md-nav'),
                lg_items = carousel.data('lg-items'),
                lg_nav = carousel.data('lg-nav'),
                center = carousel.data('center'),
                dots_global = carousel.data('dots'),
                xxs_dots = carousel.data('xxs-dots'),
                xs_dots = carousel.data('xs-dots'),
                sm_dots = carousel.data('sm-dots'),
                md_dots = carousel.data('md-dots'),
                lg_dots = carousel.data('lg-dots');

            carousel.owlCarousel({
                autoplayHoverPause: autoplay_hover_pause,
                loop: (loop ? loop : false),
                items: (items_general ? items_general : 1),
                lazyLoad: true,
                margin: (margin ? margin : 0),
                autoplay: (autoplay ? autoplay : false),
                autoplayTimeout: (autoplayTimeout ? autoplayTimeout : 1000),
                smartSpeed: (smartSpeed ? smartSpeed : 250),
                dots: (dots_global ? dots_global : false),
                nav: (nav_general ? nav_general : false),
                navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
                navSpeed: (navSpeed ? navSpeed : false),
                center: (center ? center : false),
                responsiveClass: true,
                responsive: {
                    0: {
                        items: ( xxs_items ? xxs_items : (items_general ? items_general : 1)),
                        nav: ( xxs_nav ? xxs_nav : (nav_general ? nav_general : false)),
                        dots: ( xxs_dots ? xxs_dots : (dots_global ? dots_global : false))
                    },
                    480: {
                        items: ( xs_items ? xs_items : (items_general ? items_general : 1)),
                        nav: ( xs_nav ? xs_nav : (nav_general ? nav_general : false)),
                        dots: ( xs_dots ? xs_dots : (dots_global ? dots_global : false))
                    },
                    768: {
                        items: ( sm_items ? sm_items : (items_general ? items_general : 1)),
                        nav: ( sm_nav ? sm_nav : (nav_general ? nav_general : false)),
                        dots: ( sm_dots ? sm_dots : (dots_global ? dots_global : false))
                    },
                    992: {
                        items: ( md_items ? md_items : (items_general ? items_general : 1)),
                        nav: ( md_nav ? md_nav : (nav_general ? nav_general : false)),
                        dots: ( md_dots ? md_dots : (dots_global ? dots_global : false))
                    },
                    1199: {
                        items: ( lg_items ? lg_items : (items_general ? items_general : 1)),
                        nav: ( lg_nav ? lg_nav : (nav_general ? nav_general : false)),
                        dots: ( lg_dots ? lg_dots : (dots_global ? dots_global : false))
                    }
                }
            });

        });
    };

    var buyTheme = function() {
        if (top.location!= self.location) {
           top.location = self.location.href;
        }
        if($('#buy_theme').length) {
            var buyBtn = $('#buy_theme');
            buyBtn.attr('href', window.location.href);
            buyBtn.on('click', function(){
                var affiliateLink = buyBtn.data('href');
                $("head").append('<meta http-equiv="refresh" content="1;url='+affiliateLink+'" />');
            });
        }
    };
	
	/* ================================
       Navbar-> Sub Navbar
    ================================= */
		
	$(".Electronics-menu").hover(function(){
  $("#Electronics").css("display", "block");
  $("#Appliances, #Recharge, #Food, #Travel, #Fashion, #Entertainment").css("display", "none")
  },function(){
  // $("#Appliances").css("display", "");
});


$(".Appliances-menu").hover(function(){
  $("#Appliances").css("display", "block");
$("#Electronics, #Recharge, #Food, #Travel, #Fashion, #Entertainment").css("display", "none")
  },function(){
  // $("#Appliances").css("display", "");
});

  $(".Recharge-menu").hover(function(){
  $("#Recharge").css("display", "block");
  $("#Electronics, #Appliances, #Food, #Travel, #Fashion, #Entertainment").css("display", "none")
  },function(){
  // $("#Recharge").css("display", "");
});
 
  $(".Food-menu").hover(function(){
  $("#Food").css("display", "block");
  $("#Electronics, #Appliances, #Recharge, #Travel, #Fashion, #Entertainment").css("display", "none")
  },function(){
  // $("#Food").css("display", "none");
});
	
  $(".Travel-menu").hover(function(){
  $("#Travel").css("display", "block");
  $("#Electronics, #Appliances, #Recharge, #Food, #Fashion, #Entertainment").css("display", "none")
  },function(){
  // $("#Travel").css("display", "none");
});
	
  $(".Fashion-menu").hover(function(){
  $("#Fashion").css("display", "block");
  $("#Electronics, #Appliances, #Recharge, #Travel, #Food, #Entertainment").css("display", "none")
  },function(){
  // $("#Fashion").css("display", "none");
});
	
	$(".Entertainment-menu").hover(function(){
  $("#Entertainment").css("display", "block");
  $("#Electronics, #Appliances, #Recharge, #Travel, #Food, #Fashion").css("display", "none")
  },function(){
  // $("#Entertainment").css("display", "none");
});
	
	/* ================================
       Deal Coupon SLider
    ================================= */
	
	$(document).ready(function() {
      var owl = $('.latest-coupons-slider');
      owl.owlCarousel({
        margin: 10,
        nav: false,
        dots: false,
        loop: false,		  
		responsive: {
    200: {
      items: 1,
      nav: false,
      dots: false,
      autoWidth: false,
      margin: 10,
    },
	1000: { 
		items: 4,
   }			
 }		  
      })

    });
	
	
	

	

    /* ================================
       When document is ready, do
    ================================= */
       
        $(document).on('ready', function() {
            buyTheme();
            preloader();
            $('[data-toggle="tooltip"]').tooltip();
            html_direction();
            background_color();
            background_image();
            link_void();
            click_back();
            bugfix();
            navbar_js();
            share_social();
            add_favorite();
            owl_carousel();
            toogle_class();
            countdown();
            data_rating();
            do_rating();
            countdown();
            cart_delete_item();
        });
        
    /* ================================
       When document is loading, do
    ================================= */
        
        $(window).on('load', function() {
            preloader();
            navbar_resize_load();
            product_slider();
        }); 

    /* ================================
       When Window is resizing, do
    ================================= */
        
        $(window).on('resize', function() {
            navbar_resize_load();
        });

    /* ================================
       When document is Scrollig, do
    ================================= */
        
        $(window).on('scroll', function() {
            back_to_top();
        });

    
})(jQuery);






	/* ================================
       DealSharebtn
    ================================= */
	
	$(document).click(function(){
		});
		
		
	function shareBtn(e) {
	  var x = document.getElementById("share-btn-dropdown");
	  if (x.style.display === "flex") {
		x.style.display = "none";
	  } else {
		x.style.display = "flex";
	  }	
		e.stopPropagation();
	}


/* ================================
       homepageSlider
    ================================= */
	
	$(document).ready(function() {
      var owl = $('.main-banner');
      owl.owlCarousel({
        margin: 10,
        nav: true,
		  navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
		  dots: true,
		  loop: true,
		  autoplay:true,
		  autoplayTimeout:5000,
		  autoplayHoverPause:true,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 1
          },
          2800: {
            items: 1
          }
        }
      })

    });

/* ================================
       topHeaderFix
    ================================= */

$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 50) {
        $(".top-header-fix").addClass("top-header-animate");
    }
		else{
			$(".top-header-fix").removeClass("top-header-animate");
		}
});

/* ================================
       textCopied
    ================================= */

	function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
			var x = document.getElementById("copied_popup");
  x.className = "show";
  setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}

/* ================================
       incomeCalculator
    ================================= */


$(document).ready(function () {
  var navListItems = $('div.setup-panel div a'),
          allWells = $('.setup-content'),
          allNextBtn = $('.nextBtn');

  allWells.hide();

  navListItems.click(function (e) {
      e.preventDefault();
      var $target = $($(this).attr('href')),
              $item = $(this);

      if (!$item.hasClass('disabled')) {		 
          navListItems.removeClass('btn-primary').addClass('btn-default');
          $item.addClass('btn-primary');
          allWells.hide();
          $target.show();
          $target.find('input:eq(0)').focus();
      }
  });

  allNextBtn.click(function(){
      var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='number']"),
          isValid = true;

      $(".form-group").removeClass("has-error");
      for(var i=0; i<curInputs.length; i++){
          if (!curInputs[i].validity.valid){
              isValid = false;
              $(curInputs[i]).closest(".form-group").addClass("has-error");
          }
      }

      if (isValid)
          nextStepWizard.removeClass('disabled').trigger('click');
  });

  $('div.setup-panel div a.btn-primary').trigger('click');
});
	
	$('.step1_input').on('keyup', function(e){
		
		if(this.value>10){
			this.value='10';
		} 
		else if(this.value<0){
			this.value='0';
		}
		
		});
	
	//level1
	$('input[type="number"]').on('keyup', function(e){
	
		
		var level1_input = $('input[type="number"]#level1_input').val();
	$('input[type="number"]#level1_res').val(level1_input);
		//level2
	var level2_input = $('input[type="number"]#level2_input').val();
	var level1_res = parseInt($('input[type="number"]#level1_res').val()) || 0;
		
		
	$('input[type="number"]#level2_res').val(level1_res*parseInt(level2_input));
		//level3
		
		var level2_res = parseInt($('input[type="number"]#level2_res').val()) || 0;
	var level3_input = $('input[type="number"]#level3_input').val();
	$('input[type="number"]#level3_res').val(level2_res*parseInt(level3_input));
		
		//level4
		var level3_res = parseInt($('input[type="number"]#level3_res').val()) || 0;
	var level4_input = $('input[type="number"]#level4_input').val();
	$('input[type="number"]#level4_res').val(level3_res*parseInt(level4_input));
		
		//level5
		var level4_res = parseInt($('input[type="number"]#level4_res').val()) || 0;
	var level5_input = $('input[type="number"]#level5_input').val();
	$('input[type="number"]#level5_res').val(level4_res*parseInt(level5_input));
		
		//level6
		var level5_res = parseInt($('input[type="number"]#level5_res').val()) || 0;
	var level6_input = $('input[type="number"]#level6_input').val();
	$('input[type="number"]#level6_res').val(level5_res*parseInt(level6_input));
		
		var level6_res = parseInt($('input[type="number"]#level6_res').val()) || 0;
		var sum = level1_res+level2_res+level3_res+level4_res+level5_res+level6_res;

	$('#totalRefer').text(sum);
		
		
		var grocery = parseInt($('input[type="number"]#grocery').val()) || 0;
		var medicine = parseInt($('input[type="number"]#medicine').val()) || 0;
		var appareal = parseInt($('input[type="number"]#appareal').val()) || 0;
		var footwear = parseInt($('input[type="number"]#footwear').val()) || 0;
		var travel = parseInt($('input[type="number"]#travel').val()) || 0;
		var others = parseInt($('input[type="number"]#other').val()) || 0;
		var totalExpence = grocery+medicine+appareal+footwear+travel+others
		
		$('#totalExpence').text(totalExpence);
		
		var totalReferIncome = sum * (totalExpence/6000);
		
		$('#totalReferIncome').text(totalReferIncome);
		
	});	
	
$('#resetCalc').on("click",function(){
	$('input[type="number"]').val("");
	$('#totalExpence').text("");
	$('#totalRefer').text("");
	$('div.setup-panel div a[href="#step-2"]').addClass("disabled");
	$('div.setup-panel div a[href="#step-3"]').addClass("disabled");
	$('div.setup-panel div a[href="#step-1"]').trigger('click');
});


/* ================================
       Sticky Store Header
    ================================= */
function sticky_relocate() {
  var window_top = $(window).scrollTop();
  var div_top = $('#sticky-anchor').offset().top;
  if (window_top > div_top) {
    $('#sticky-store').addClass('stick');
  } else {
    $('#sticky-store').removeClass('stick');
  }
}

$(function() {
  $(window).scroll(sticky_relocate);
  sticky_relocate();
});

