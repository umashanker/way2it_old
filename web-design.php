<?php include 'header.php';?>

<body>


   <div id="banner-area" class="banner-area" style="background-image:url(images/banner/banner5.jpg)">
      <div class="container">
         <div class="row">
            <div class="col-sm-12">
               <div class="banner-heading">
                  <h1 class="banner-title">Website Design</h1>
                  <ol class="breadcrumb">
                     <li><a href="home">Home</a></li>
                     <li>Website Design</li>
                  </ol>
               </div>
            </div>
            <!-- Col end -->
         </div>
         <!-- Row end -->
      </div>
      <!-- Container end -->
   </div>
   <!-- Banner area end -->


   <section id="main-container" class="main-container">
      <div class="container">
       	  <div class="row">
            <div class="col-md-4 align-self-md-center">
               <img src="images/web-design/customize.png" alt="customized" class="img-fluid">
            </div>
            <!-- Col End -->
            <div class="col-md-8">
               <div class="tw-about-bin">
                  <h2 class="column-title">Customized</h2>
				   <p><b>Company give you a good connectivity and as well as good performation</b></p>
                  <span class="animate-border tw-mb-30"></span>
                  <p>We give you a Unique design. With a custom web design it is created just for your business. Your website will be different from anyone else's. By hiring the right web team, it will be constructed so it is search engine friendly. How the background coding of your website is done will influence your success in the search engines. The website will be more adaptable for you and your business or company's needs.</p>
               </div>
               <!-- About PinkPixels End -->
            </div>
            <!-- Col End -->
			 
			  
            <!-- Col End -->
		  </div>  <br>
         
		  <div class="row">
            <div class="col-md-4 align-self-md-center">
               <img src="images/web-design/responsive.png" alt="responsive" class="img-fluid">
            </div>
            <!-- Col End -->
            <div class="col-md-8">
               <div class="tw-about-bin">
                  <h2 class="column-title">Responsive</h2>
				   <p><b>User friendly and effective websites for multiple device</b></p>
                  <span class="animate-border tw-mb-30"></span>
                  
                  <p>More than 90% of your website visitor or follower are now using mobile and tablets. Our team develops fully responsive websites with an intelligent user interface that adapts to varied screen resolutions and deliver an amazing user experience regardless of the device and browsing environment. This website will be open on any devices</p>
               </div>
               <!-- About PinkPixels End -->
            </div>
            <!-- Col End -->
			 
			  
            <!-- Col End -->
         </div>	<br>
		  
		  <div class="row">
            <div class="col-md-4 align-self-md-center">
               <img src="images/web-design/effective.png" alt="effective" class="img-fluid">
            </div>
            <!-- Col End -->
            <div class="col-md-8">
               <div class="tw-about-bin">
                  <h2 class="column-title">Effective</h2>
				   <p><b>Effective website helpfull for your marketing value's and clients</b></p>
                  <span class="animate-border tw-mb-30"></span>

                  <p>If you have an effective website then the website spot of all your marketing efforts. Our web design team visualizes the site with a marketing perspective to build a website that not just looks great but can convert visitors into customers. And after that, your website gives you a better response.</p>
               </div>
               <!-- About PinkPixels End -->
            </div>
            <!-- Col End -->
			 
			  
            <!-- Col End -->
         </div>	<br>
		  
		  <div class="row">
            <div class="col-md-4 align-self-md-center">
               <img src="images/web-design/beautiful.png" alt="beautiful" class="img-fluid">
            </div>
            <!-- Col End -->
            <div class="col-md-8">
               <div class="tw-about-bin">
                  <h2 class="column-title">Beautiful</h2>
				   <p><b>Beautiful and Unique website give better experience.</b></p>
                  <span class="animate-border tw-mb-30"></span>
                  <p>We are creative and innovative in web design, development, and our primary focus is to add new features that can lay down to ensure success for your online business. Our creative designing team spends the time to understand your business objectives & target niche before starting the designing process.</p>
               </div>
               <!-- About PinkPixels End -->
            </div>
            <!-- Col End -->
			 
			  
            <!-- Col End -->
         </div>
		  
      </div>
      <!-- Container end -->
   </section>


</body>
<?php include 'footer.php';?>