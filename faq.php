<?php include 'header.php';?>

<body>


   <div id="banner-area" class="banner-area" style="background-image:url(images/banner/banner5.jpg)">
      <div class="container">
         <div class="row">
            <div class="col-sm-12">
               <div class="banner-heading">
                  <h1 class="banner-title">FAQ</h1>
                  <ol class="breadcrumb">
                     <li><a href="home">Home</a></li>
                     <li>faq</li>
                  </ol>
               </div>
            </div>
            <!-- Col end -->
         </div>
         <!-- Row end -->
      </div>
      <!-- Container end -->
   </div>
   <!-- Banner area end -->


   <section id="main-container" class="main-container">
      <div class="container">
         <div class="row">
            <div class="col-lg-8 col-md-12">
               <div class="tw-faq-content">
                  <div class="section-heading">
                     <h3>FAQ</h3>
                     <span class="tw-mt-20"></span>
                  </div>
                  <div id="accordion" role="tablist">
                     <div class="card ">
                        <div class="card-header" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"
                           aria-controls="collapseOne">
                           <h4>
                              <a>
                                 What is a page layout?
                                 <i class="faq-indicator fa fa-minus"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne">
                           <div class="card-block">
                              Page layout is the geometry of a page. It will help us to define how many regions will appear and how they are visually organized.
                           </div>
                        </div>
                     </div>
                     <div class="card ">
                        <div class="card-header" role="tab" id="headingtwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                           aria-controls="collapseTwo">
                           <h4>
                              <a class="collapsed">
                                 What about payment gateway and shipping integration on eCommerce websites?
                              <i class="faq-indicator fa fa-plus"></i>
                           </a>
                           </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                           <div class="card-block">
                              We can integrate the most common payment gateways (PayPal,PayuMoney,Cca Avenue) on our eCommerce websites, however you might have a specific request and it might take some time to study that specific case. We are open to discuss with you and help you during the integration process. It may impact the amount of time we initially defined at the start of the project.
                           </div>
                        </div>
                     </div>
                     <div class="card ">
                        <div class="card-header" role="tab" id="headingthree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"
                           aria-expanded="false" aria-controls="collapseThree">
                           <h4>
                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                 Will we need to have a face-to-face meeting to begin the project?
                                 <i class="faq-indicator fa fa-plus"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingthree">
                           <div class="card-block">
                              In the majority of cases a meeting isn’t necessary and most of our websites are generally completed with contact via email and telephone only. We place suggested website designs online via, our testing server for you to view the work in progress. This way We can have regular communication to ensure you are getting exactly what you expect from the our team.
                           </div>
                        </div>
                     </div>
					  <div class="card ">
                        <div class="card-header" role="tab" id="headingfour" data-toggle="collapse" data-parent="#accordion" href="#collapsefour"
                           aria-expanded="false" aria-controls="collapsefour">
                           <h4>
                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="true" aria-controls="collapsefour">
                                 Do you redesign existing websites?
                                 <i class="faq-indicator fa fa-plus"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                           <div class="card-block">
                              Yes. We can redesign existing sites and retain your organization's style or give you a completely new look.
                           </div>
                        </div>
                     </div>
					  <div class="card ">
                        <div class="card-header" role="tab" id="headingFive" data-toggle="collapse" data-parent="#accordion" href="#collapseFive"
                           aria-expanded="false" aria-controls="collapseFive">
                           <h4>
                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                 I am totally new to this "website thing". How does the whole process work?
                                 <i class="faq-indicator fa fa-plus"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                           <div class="card-block">
                              Never fear, that's why we are here. You can learn about our time proven process in our process section.
                           </div>
                        </div>
                     </div>
					  <div class="card ">
                        <div class="card-header" role="tab" id="headingSix" data-toggle="collapse" data-parent="#accordion" href="#collapseSix"
                           aria-expanded="false" aria-controls="collapseSix">
                           <h4>
                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                                 How much does a website cost?
                                 <i class="faq-indicator fa fa-plus"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                           <div class="card-block">
                              The cost of a website can vary depending on various factors, just like the cost of a house may vary. Though our website projects generally start in the <strong>Rs. 1500</strong> range for basic business sites and range upward depending on your unique needs.
                           </div>
                        </div>
                     </div>
					  <div class="card ">
                        <div class="card-header" role="tab" id="headingSeven" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven"
                           aria-expanded="false" aria-controls="collapseSeven">
                           <h4>
                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                                 How does the payment process work?
                                 <i class="faq-indicator fa fa-plus"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                           <div class="card-block">
                              The project starts with a 50% deposit. After design sign off and before we move into programming, we collect 25%. Once we have completed and fulfilled our scope, the final 25% is collected and your website is then scheduled for launch.
                           </div>
                        </div>
                     </div>
					  <div class="card ">
                        <div class="card-header" role="tab" id="headingEight" data-toggle="collapse" data-parent="#accordion" href="#collapseEight"
                           aria-expanded="false" aria-controls="collapseEight">
                           <h4>
                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="true" aria-controls="collapseEight">
                                 What kind of businesses do you work with?
                                 <i class="faq-indicator fa fa-plus"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                           <div class="card-block">
                              We work with a broad range of company types [small start-ups, large corporations, nonprofits, B2B, B2C and more] across many business industries [technology, food, apparel, health + beauty, camps, travel, finance, arts, fair trade, and more.] 
Over the years, we've helped businesses improve customer service, market their products, and attract customers. Functionality requests range from basic to advanced. Our team takes the time to get to know your industry, organization, and competitors to ensure your site supports all of your goals.
                           </div>
                        </div>
                     </div>
					  <div class="card ">
                        <div class="card-header" role="tab" id="headingNine" data-toggle="collapse" data-parent="#accordion" href="#collapseNine"
                           aria-expanded="false" aria-controls="collapseNine">
                           <h4>
                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="true" aria-controls="collapseNine">
                                 How long does it take to build a website?
                                 <i class="faq-indicator fa fa-plus"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                           <div class="card-block">
                              Our standard websites take approximately 10-15 days to create. Our E-commerce (online store) websites take approximately 40 days to create. This time will vary from project to project.
                           </div>
                        </div>
                     </div>
					  <div class="card ">
                        <div class="card-header" role="tab" id="headingTen" data-toggle="collapse" data-parent="#accordion" href="#collapseTen"
                           aria-expanded="false" aria-controls="collapseTen">
                           <h4>
                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="true" aria-controls="collapseTen">
                                 Who will I work with during the project?
                                 <i class="faq-indicator fa fa-plus"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
                           <div class="card-block">
                              This is a great question to ask and you should ask it of any web design and development firm you are considering. Many firms will farm out work to freelancers or interns. Some firms give little or no access to client’s who want to speak directly with their designers or developers. Our clients work directly with our tight knit crew. Who that will be depends on your project needs and what stage of the process you are in during the project.
                           </div>
                        </div>
                     </div>
					  <div class="card ">
                        <div class="card-header" role="tab" id="headingEleven" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven"
                           aria-expanded="false" aria-controls="collapseEleven">
                           <h4>
                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="true" aria-controls="collapseEleven">
                                 What platform do you build your websites on?
                                 <i class="faq-indicator fa fa-plus"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven">
                           <div class="card-block">
                              We build all of our websites on the Efficient in Microsoft applications and in-depth Microsoft architecture. All .NET based technologies and Microsoft environment programming including SharePoint is managed by this team.
                           </div>
                        </div>
                     </div>
					  <div class="card ">
                        <div class="card-header" role="tab" id="headingTwelve" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve"
                           aria-expanded="false" aria-controls="collapseTwelve">
                           <h4>
                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve" aria-expanded="true" aria-controls="collapseTwelve">
                                 Can you help me write content for my website?
                                 <i class="faq-indicator fa fa-plus"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseTwelve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwelve">
                           <div class="card-block">
                              Yes. We include copy writing and editing in all of our proposals. We also build your sitemap and help structure the foundation of your website in the planning stages.
                           </div>
                        </div>
                     </div>
					  <div class="card ">
                        <div class="card-header" role="tab" id="headingThirteen" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen"
                           aria-expanded="false" aria-controls="collapseThirteen">
                           <h4>
                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen" aria-expanded="true" aria-controls="collapseThirteen">
                                 Can you help me source photos for my website?
                                 <i class="faq-indicator fa fa-plus"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseThirteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
                           <div class="card-block">
                              Yes. We will help you source stock photos from sites such as www.crestock.com and www.istock.com. We also have photographers and videographers available to produce custom photography and videos for your website.
                           </div>
                        </div>
                     </div>
					  <div class="card ">
                        <div class="card-header" role="tab" id="headingFourteen" data-toggle="collapse" data-parent="#accordion" href="#collapseFourteen"
                           aria-expanded="false" aria-controls="collapseFourteen">
                           <h4>
                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourteen" aria-expanded="true" aria-controls="collapseFourteen">
                                 Do I own my website?
                                 <i class="faq-indicator fa fa-plus"></i>
                              </a>
                           </h4>
                        </div>
                        <div id="collapseFourteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFourteen">
                           <div class="card-block">
                              Yes! Everything that we build will be 100% owned by you. Most of our clients stay with us for the life of the website. Our team is just irresistible that way. But if for any reason you decide you want to take your site to another hosting service and get another company to service the site, we will happily assist you in making the transition as effortlessly and efficiently as possible. The website is yours after all, and we want you to take it wherever you go.
                           </div>
                        </div>
                     </div>
					  
                     <!-- panel-group -->
                  </div>
                  <!-- End Accordion -->
               </div>
               <!-- Faq content end -->
            </div>
            <!-- Content Col end -->
            <div class="col-lg-4 col-md-12">
               <div class="sidebar sidebar-right faq-right">
                  <div class="widget widget-adds">
                     <a href="#">
                        <img src="images/news/ad.jpg" alt="" class="img-fluid">
                     </a>
                  </div>
               </div>
               <!-- End Sidebar Right -->
            </div>
            <!-- Sidebar Col end -->

         </div>
         <!-- Main row end -->
      </div>
      <!-- Container end -->
   </section>


</body>
<?php include 'footer.php';?>